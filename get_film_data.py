import requests
import json

API_KEY = "2412e5503277c9466e1bcd9f1258d52f"

print("Please enter a film ID:")
MOVIE_ID = input()

reqFilmData = requests.get(f'https://api.themoviedb.org/3/movie/{MOVIE_ID}?api_key={API_KEY}')
filmData = json.loads(reqFilmData.text)

reqCreditData = requests.get(f'https://api.themoviedb.org/3/movie/{MOVIE_ID}/credits?api_key={API_KEY}')
creditData = json.loads(reqCreditData.text)

CREDIT_ID = creditData['id']
reqCastData = requests.get(f'https://api.themoviedb.org/3/credit/{CREDIT_ID}?api_key={API_KEY}')
castData = json.loads(reqCastData.text)


print("Title: " + filmData['original_title'])
print("Genre: " + filmData['genres'][0]['name'])
print("Release Date: " + filmData['release_date'])
print("Production Company: " + filmData['production_companies'][0]['name'])
print("Revenue: " + str(filmData['revenue']))

for crew in creditData['crew']:
    if crew['job'] == 'Director':
        print("Director: " + crew['name'])

print("Cast:")
for actor in creditData['cast']:
    print(actor['name'] + " as " + actor['character'])