--Display Details for Film: Fight Club
SELECT F.TITLE Film,
        A.FIRSTNAME Actor_Firstname,
        A.LASTNAME Actor_Lastname,
        D.FIRSTNAME Director_Firstname,
        D.LASTNAME Director_Lastname,
        St.NAME Studio,
        I.RATINGS IMDB_Rating_0_TO_10,
        I.NUMBEROFRATINGS IMDB_Number_Of_Ratings
FROM FILM F, CAST C, ACTOR A, DIRECTOR D, IMDB I, STUDIO St
WHERE F.FILM_ID=2
    AND C.FILM_ID= F.FILM_ID
    AND A.ACTOR_ID=C.ACTOR_ID
    AND F.DIRECTOR_ID=D.DIRECTOR_ID
    AND I.IMDB_ID=F.IMDB_ID
    AND F.STUDIO_ID=St.NAME;