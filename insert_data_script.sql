-- restartable insert script

-- Lösche zuerst alle Daten aus den abhängigen Tabellen:
delete from film;
delete from soundtrack;
delete from cast;
  -- Lösche dann alle Daten aus den unabhängigen Tabellen:
delete from music;
delete from actor;
delete from imdb;
delete from director;
delete from studio;
delete from userAccess;

-- insert data 

-- userAccess
INSERT INTO userAccess(userName, password, role) VALUES ('ChristianMartin', 'abc123', 'db_admin');
INSERT INTO userAccess(userName, password, role) VALUES ('LennartGastler', 'def456', 'db_admin');
INSERT INTO userAccess(userName, password, role) VALUES ('MaximilianChristoph', 'hij789', 'db_user');
INSERT INTO userAccess(userName, password, role) VALUES ('EliasMerzhaeuser', 'klm123', 'db_user');

-- studio

INSERT INTO studio(name , country) VALUES('Fox 2000 Pictures' , 'USA');
INSERT INTO studio(name , country) VALUES('Twentieth Century Fox' , 'USA');
INSERT INTO studio(name , country) VALUES('Paramount Pictures' , 'USA');
INSERT INTO studio(name , country) VALUES('Columbia Pictures' , 'USA');
INSERT INTO studio(name , country) VALUES('Zoetrope' , 'USA');

-- director

INSERT INTO director(director_id , firstname , lastname , birthday) VALUES(1, 'David', 'Fincher', TO_DATE('28.08.1962', 'DD.MM.YYYY'));
INSERT INTO director(director_id , firstname , lastname , birthday) VALUES(2, 'James', 'Cameron', TO_DATE('16.08.1954', 'DD.MM.YYYY'));
INSERT INTO director(director_id , firstname , lastname , birthday) VALUES(3, 'Christopher', 'Nolan', TO_DATE('30.07.1970', 'DD.MM.YYYY'));
INSERT INTO director(director_id , firstname , lastname , birthday) VALUES(4, 'Robert', 'Luketic', TO_DATE('01.11.1973', 'DD.MM.YYYY'));
INSERT INTO director(director_id , firstname , lastname , birthday) VALUES(5, 'Francis', 'Ford Coppola', TO_DATE('07.04.1939', 'DD.MM.YYYY'));
INSERT INTO director(director_id , firstname , lastname , birthday) VALUES(6, 'Roland', 'Emmerich', TO_DATE('10.11.1955', 'DD.MM.YYYY'));

-- imdb

INSERT INTO imdb(ratings , numberOfRatings , imdb_id) VALUES(8.8, 1685561, 'tt0137523');
INSERT INTO imdb(ratings , numberOfRatings , imdb_id) VALUES(7.8, 1048245, 'tt0499549');
INSERT INTO imdb(ratings , numberOfRatings , imdb_id) VALUES(8.6, 1306186, 'tt0816692');
INSERT INTO imdb(ratings , numberOfRatings , imdb_id) VALUES(6.5, 214107, 'tt0478087');
INSERT INTO imdb(ratings , numberOfRatings , imdb_id) VALUES(8.5, 551237, 'tt0078788');
INSERT INTO imdb(ratings , numberOfRatings , imdb_id) VALUES(6.4, 192283, 'tt2334879');

-- actor

INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(1, 'Edward', 'Norton', TO_DATE('18.08.1969', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(2, 'Brad', 'Pitt', TO_DATE('18.12.1963', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(3, 'Helena', 'Bonham Carter', TO_DATE('26.05.1966', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(4, 'Meat', 'Loaf', TO_DATE('27.09.1947', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(5, 'Jared', 'Leto', TO_DATE('26.12.1971', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(6, 'Sam', 'Worthington', TO_DATE('02.08.1976', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(7, 'Zoe', 'Saldana', TO_DATE('19.06.1978', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(8, 'Stephen', 'Lang', TO_DATE('11.07.1952', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(9, 'Sigourney', 'Weaver', TO_DATE('08.10.1949', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(10, 'Joel David', 'Moore', TO_DATE('25.09.1977', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(11, 'Matthew', 'McConaughey', TO_DATE('04.11.1969', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(12, 'Jessica', 'Chastain', TO_DATE('24.03.1977', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(13, 'Anne', 'Hathaway', TO_DATE('12.11.1982', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(14, 'Michael', 'Caine', TO_DATE('14.03.1933', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(15, 'Casey', 'Affleck', TO_DATE('12.08.1975', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(16, 'Marlon ', 'Brando', TO_DATE('03.04.1924', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(17, 'Robert ', 'Duvall', TO_DATE('05.01.1931', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(18, 'Martin ', 'Sheen', TO_DATE('03.08.1940', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(19, 'Frederic ', 'Forrest', TO_DATE('23.12.1936', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(20, 'Albert ', 'Hall', TO_DATE('10.11.1937', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(21, 'Channing ', 'Tatum', TO_DATE('26.04.1980', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(22, 'Jamie ', 'Foxx', TO_DATE('13.12.1967', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(23, 'Joey ', 'King', TO_DATE('30.07.1999', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(24, 'Maggie ', 'Gyllenhaal', TO_DATE('16.11.1977', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(25, 'Richard ', 'Jenkins', TO_DATE('04.05.1947', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(26, 'Jim ', 'Sturgess', TO_DATE('16.05.1978', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(27, 'Kevin ', 'Spacey', TO_DATE('26.07.1959', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(28, 'Kate ', 'Bosworth', TO_DATE('02.01.1983', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(29, 'Aaron ', 'Yoo', TO_DATE('12.05.1979', 'DD.MM.YYYY'));
INSERT INTO ACTOR(actor_id , firstname , lastname , birthday) VALUES(30, 'Liza ', 'Lapira', TO_DATE('03.12.1981', 'DD.MM.YYYY'));

-- music

INSERT INTO music(music_id , title , composer) VALUES(1, 'Filmmusik', 'Dave Sardy');
INSERT INTO music(music_id , title , composer) VALUES(2, 'You Don’t Dream in Cryo. …', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(3, 'Jake Enters His Avatar World', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(4, 'Pure Spirits of the Forest', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(5, 'The Bioluminescence of the Night', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(6, 'Becoming One of „The People“ Becoming One with Neytiri', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(7, 'Climbing Up „Iknimaya – The Path to Heaven“', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(8, 'Jake’s First Flight', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(9, 'Scorched Earth', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(10, 'Quaritch', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(11, 'The Destruction of Hometree', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(12, 'Shutting Down Grace’s Lab', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(13, 'Gathering All the Na’vi Clans for Battle', 'James Horner');
INSERT INTO music(music_id , title , composer) VALUES(14, 'I See You (Theme from Avatar)', 'Leona Lewis');
INSERT INTO music(music_id , title , composer) VALUES(15, 'Cornfield Chase', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(16, 'Dust', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(17, 'Day One', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(18, 'Stay', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(19, 'Message from Home', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(20, 'The Wormhole', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(21, 'Mountains', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(22, 'Afraid of Time', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(23, 'A Place Among the Stars', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(24, 'Running Out', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(25, 'Im Going Home', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(26, 'Coward', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(27, 'Detach', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(28, 'S.T.A.Y.', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(29, 'Where Were Going', 'Hans Zimmer');
INSERT INTO music(music_id , title , composer) VALUES(30, 'Filmmusik', 'Dust Brothers');
INSERT INTO music(music_id , title , composer) VALUES(31, 'Filmmusik', 'Carmine Coppola');
INSERT INTO music(music_id , title , composer) VALUES(32, 'Filmmusik', 'Harald Kloser');
INSERT INTO music(music_id , title , composer) VALUES(33, 'Filmmusik', 'Thomas Wander');

-- film

INSERT INTO film(film_id , title , genre , releaseYear , director_id , studio_id , budget , imdb_id , revenue) VALUES(1 , 'Fight Club', 'Drama' , TO_DATE('1999', 'YYYY') , 1 , 'Fox 2000 Pictures', 63000000 , 'tt0137523', 100851705);
INSERT INTO film(film_id , title , genre , releaseYear , director_id , studio_id , budget , imdb_id , revenue) VALUES(2 , 'Avatar ', 'Action' , TO_DATE('2009', 'YYYY') , 2 , 'Twentieth Century Fox', 237000000 , 'tt0499549', 3177085575);
INSERT INTO film(film_id , title , genre , releaseYear , director_id , studio_id , budget , imdb_id , revenue) VALUES(3 , 'Interstellar', 'Adventure' , TO_DATE('2014', 'YYYY') , 3 , 'Paramount Pictures', 165000000 , 'tt0816692', 712538675);
INSERT INTO film(film_id , title , genre , releaseYear , director_id , studio_id , budget , imdb_id , revenue) VALUES(4 , '21', 'Crime' , TO_DATE('2008', 'YYYY') , 4 , 'Columbia Pictures', 35000000 , 'tt0478087', 188357511);
INSERT INTO film(film_id , title , genre , releaseYear , director_id , studio_id , budget , imdb_id , revenue) VALUES(5 , 'Apocalypse Now', 'Drama' , TO_DATE('1979', 'YYYY') , 5 , 'Zoetrope', 31500000 , 'tt0078788', 89812040);
INSERT INTO film(film_id , title , genre , releaseYear , director_id , studio_id , budget , imdb_id , revenue) VALUES(6 , 'White House Down', 'Action' , TO_DATE('2013', 'YYYY') , 6 , 'Columbia Pictures', 150000000 , 'tt2334879', 233282087);


-- cast

INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('The Narrator', 1, 1, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Robert Bob Paulson', 1, 2, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Marla Singer', 1, 3, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Robert Bob Paulson', 1, 4, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Angel Face', 1, 5, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Jake Sully', 2, 6, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Princess Neytiri', 2, 7, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Col. Quaritch', 2, 8, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Dr. Grace Augustine', 2, 9, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Norm Spellman', 2, 10, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Joseph Coop Cooper', 3, 11, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Murphy Murph Cooper', 3, 12, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Dr. Amelia Brand', 3, 13, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Professor John Brand', 3, 14, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Tom Cooper', 3, 15, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Colonel Walter E. Kurtz', 5, 16, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Lieutenant Colonel Bill Kilgore', 5, 17, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Captain Benjamin L. Willard', 5, 18, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Jay Chef Hicks', 5, 19, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Chief Phillips', 5, 20, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Cale', 6, 21, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('James Sawyer', 6, 22, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Emily', 6, 23, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Finnerty', 6, 24, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Raphelson', 6, 25, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Ben Campbell', 4, 26, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Micky Rosa', 4, 27, 1);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Jill Taylor', 4, 28, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Choi', 4, 29, 0);
INSERT INTO cast(role , film_id , actor_id , leadingRole) VALUES('Kianna', 4, 30, 0);

-- soundtrack

INSERT INTO soundtrack(film_id , music_id) VALUES(4 , 1);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 2);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 3);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 4);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 5);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 6);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 7);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 8);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 9);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 10);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 11);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 12);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 13);
INSERT INTO soundtrack(film_id , music_id) VALUES(2 , 14);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 15);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 16);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 17);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 18);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 19);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 20);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 21);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 22);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 23);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 24);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 25);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 26);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 27);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 28);
INSERT INTO soundtrack(film_id , music_id) VALUES(3 , 29);
INSERT INTO soundtrack(film_id , music_id) VALUES(1 , 30);
INSERT INTO soundtrack(film_id , music_id) VALUES(5 , 31);
INSERT INTO soundtrack(film_id , music_id) VALUES(6 , 32);
INSERT INTO soundtrack(film_id , music_id) VALUES(6 , 33);

commit;