-- -------------------------------------------------
-- 4.1 create tables script
-- Restart-fähiges Skript zum Erstellen der Tabellen
-- -------------------------------------------------
-- DELETION PROCESS
-- ----------------
-- Lösche zuerst alle Daten aus den abhängigen Tabellen:
delete from soundtrack;
-- stage 3
delete from cast;
-- stage 2
delete from film;
-- stage 1
  -- Lösche dann alle Daten aus den unabhängigen Tabellen:
delete from music;
-- stage 1
delete from actor;
-- stage 1
delete from imdb;
-- stage 1
delete from director;
-- stage 1
delete from studio;
-- stage 1
delete from userAccess;
-- stage 1
  -- drop all the constraints: switches off all dependencies, makes all tables independent.
alter table film drop constraint director_fk;
alter table film drop constraint studio_fk;
alter table film drop constraint imdb_fk;
alter table cast drop constraint film_fk;
alter table cast drop constraint actor_fk;
alter table userAccess drop constraint userAccess_u1;
alter table soundtrack drop constraint film_fk_soundtrack;
alter table soundtrack drop constraint music_fk;

  -- delete primary keys
alter table studio drop constraint studio_pk;
alter table director drop constraint director_pk;
alter table imdb drop constraint imdb_pk;
alter table film drop constraint film_pk;
alter table actor drop constraint actor_pk;
alter table music drop constraint music_pk;
-- drop the tables in correct sequence: drop dependent tables first
drop table soundtrack;
drop table cast;
drop table film;
-- then drop the independent tables
drop table music;
drop table actor;
drop table imdb;
drop table director;
drop table studio;
drop table userAccess;
-- CREATION PROCESS
-- ----------------
-- create tables in correct sequence
-- first: create the independent tables
create table userAccess (
  userName varchar(40) not null,
  password varchar(40) not null,
  role varchar(40)
);
create table studio (
  name varchar(40) not null,
  country varchar(40)
);
create table director (
  director_id number not null,
  firstname varchar(40) not null,
  lastname varchar(40) not null,
  birthday date
);
create table imdb (
    imdb_id char(9) not null,
    ratings double precision,
    numberOfRatings number
);
create table film (
  film_id number not null,
  title varchar(40),
  genre varchar(40),
  releaseYear date,
  director_id number,
  studio_id varchar(40),
  budget number,
  revenue number,
  imdb_id char(9)
);
create table actor (
  actor_id number not null,
  firstname varchar(40) not null,
  lastname varchar(40) not null,
  birthday date
);
create table cast (
  film_id number not null,
  actor_id number not null,
  role varchar(40),
  leadingRole number default 0 not null check (leadingRole in (0, 1))
);
create table music (
  music_id number not null,
  title varchar(80),
  composer varchar(80)
);
create table soundtrack (
  film_id number not null,
  music_id number not null
);
-- create primary keys here, and give them names. Makes later deletion easier:

alter table studio add constraint studio_pk primary key (name);
alter table director add constraint director_pk primary key (director_id);
alter table imdb add constraint imdb_pk primary key (imdb_id);
alter table film add constraint film_pk primary key (film_id);
alter table actor add constraint actor_pk primary key (actor_id);
alter table music add constraint music_pk primary key (music_id);

-- add unique constraint: "Referenzfelder müssen zusammen eindeutig sein"

alter table userAccess add constraint userAccess_u1 unique (userName);

-- create foreign keys here, and give them names. Makes later deletion easier:

alter table film add constraint director_fk foreign key (director_id) references director (director_id);
alter table film add constraint studio_fk foreign key (studio_id) references studio (name);
alter table film add constraint imdb_fk foreign key (imdb_id) references imdb (imdb_id);
alter table cast add constraint film_fk foreign key (film_id) references film (film_id);
alter table cast add constraint actor_fk foreign key (actor_id) references actor (actor_id);
alter table soundtrack add constraint film_fk_soundtrack foreign key (film_id) references film (film_id);
alter table soundtrack add constraint music_fk foreign key (music_id) references music (music_id);

commit;