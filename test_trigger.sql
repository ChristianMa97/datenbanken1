delete FROM FILM where FILM_ID = 2;

select concat(
    'Test if FILM selects are correct: ',
    case when
        (select count(*) from FILM) = 5
        and (select count(*) from SOUNDTRACK) = 20
        and (select count(*) from CAST) = 25
        then 'passed' else 'failed' end
) from DUAL;